import React from "react";
import { Form, Input, Select, Row, Col, DatePicker } from "antd";
import styles from "../../index.module.css";

const { Option } = Select;

const SenderInformationForm = () => {
  const states = ["State 1", "State 2", "State 3", "State 4"];

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      style={{
        margin: "20px auto 0",
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Row gutter={16}>
        <Col span={12}>
          <div className={styles.groupLabel}>Sender Name</div>
          <Form.Item
            name="name"
            rules={[
              {
                required: true,
                message: "Please enter sender's name.",
              },
            ]}
            hasFeedback
          >
            <Input placeholder="Name" className={styles.groupChildFull2} />
          </Form.Item>
        </Col>

        <Col span={12}>
          <Form.Item
            name="contact"
            rules={[
              {
                required: true,
                message: "Please enter sender's contact.",
              },
            ]}
            hasFeedback
          >
            <Input
              placeholder="Contact"
              className={[styles.groupChildFull2, styles.poppinsRegular]}
            />
          </Form.Item>
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={24}>
          <Form.Item
            name="address"
            rules={[
              {
                required: true,
                message: "Please enter sender address",
              },
            ]}
            hasFeedback
          >
            <Input.TextArea
              rows={4}
              placeholder="Sender Address"
              className={styles.groupChildFullTextArea}
            />
          </Form.Item>
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={8}>
          <Form.Item
            name="postcode"
            rules={[
              {
                required: true,
                message: "Please enter postcode.",
              },
            ]}
            hasFeedback
          >
            <Input placeholder="Postcode" className={styles.groupChildFull2} />
          </Form.Item>
        </Col>

        <Col span={8}>
          <Form.Item
            name="city"
            rules={[
              {
                required: true,
                message: "Please enter city.",
              },
            ]}
            hasFeedback
          >
            <Input
              placeholder="City"
              className={[styles.groupChildFull2, styles.poppinsRegular]}
            />
          </Form.Item>
        </Col>

        <Col span={8}>
          <Form.Item
            name="state"
            rules={[
              {
                required: true,
                message: "Please enter state.",
              },
            ]}
            hasFeedback
          >
            <Input
              placeholder="State"
              className={[styles.groupChildFull2, styles.poppinsRegular]}
            />
          </Form.Item>
        </Col>
      </Row>

      <Row gutter={16}>
        <Col span={24}>
          <Form.Item
            name="date"
            rules={[
              {
                required: true,
                message: "Please select pickup datetime",
              },
            ]}
            hasFeedback
          >
            <DatePicker
              showTime
              className={styles.groupChildFullDatePicker}
              inputReadOnly
              width="10%"
              // popupStyle={{width: "90%"}}
            />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};

export default SenderInformationForm;
