import React, { useState } from "react";
import { Tabs, Button, message, Steps, theme, Row, Col, Divider,  Modal, Form, Input, Space, Radio, InputNumber } from "antd";
import "./index.css";
import SenderInformationForm from "./SenderInformationForm";
import ReceiverInformationForm from "./ReceiverInformationForm";
import JobInformationForm from "./JobInformationForm";
import JobSummaryForm from "./JobSummaryForm";
import PriceInformationForm from "./PriceInformationForm";
import { ROOT_CONSTANT } from "../../constants";
import { Navigate, useNavigate } from "react-router";

const { ddmovelogo } = ROOT_CONSTANT.IMG.ICON.MAIN;
const { blueLine } = ROOT_CONSTANT.IMG.ICON.LOGO;

const SubmitJob = (props) => {
  const { icon_left } = ROOT_CONSTANT.IMG.ICON.LOGO;
  const steps = [
    {
      title: "Sender Information",
      content: <SenderInformationForm />,
    },
    {
      title: "Receiver Information",
      content: <ReceiverInformationForm />,
    },
    {
      title: "Job Information",
      content: <JobInformationForm />,
    },
    {
      title: "Job Summary",
      content: <JobSummaryForm />,
    },
    {
      title: "Price Information",
      content: <PriceInformationForm />,
    },
  ];

  const { token } = theme.useToken();
  const [current, setCurrent] = useState(0);
  const next = () => {
    setCurrent(current + 1);
  };
  const prev = () => {
    setCurrent(current - 1);
  };
  const items = steps.map((item) => ({
    key: item.title,
    title: item.title,
  }));
  const contentStyle = {
    lineHeight: "260px",
    textAlign: "center",
    color: token.colorTextTertiary,
    marginTop: 16,
    height: "70vh",
    width: "90%",
    margin: "0 auto",
  };

  const [isModalVisible, setIsModalVisible] = useState(false);

  const handleClose = () => {
    setIsModalVisible(false);
  };

  const [name, setName] = useState('');
  const [number, setNumber] = useState('');
  const [email, setEmail] = useState('');

  const handleChange = (event) => {
      setName(event.target.value);
      setNumber(event.target.value);
      setEmail(event.target.value);
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const navigate = useNavigate()

  return (
    <>
      <h1 style={{ margin: "20px 0" }}> Submit Job Page </h1>
      <Steps current={current} items={items} responsive={false} size="50px" />

      <div style={contentStyle}>{steps[current].content}</div>
      <div
        style={{
          marginTop: 24,
        }}
      >
        {current < steps.length - 1 && (
          <Button
            type="primary"
            onClick={() => next()}
            style={{
              backgroundColor: "#0276C0",
              width: "80%",
              margin: "0 auto",
              position: "absolute",
              left: 0,
              right: 0,
              bottom: "10%",
              textAlign: "center",
              display: current === steps.length - 1 ? "none" : "block",
            }}
          >
            Next
          </Button>
        )}
        {current === steps.length - 1 && (
          <Button
            type="primary"
            onClick={showModal}
            style={{
              backgroundColor: "#0276C0",
              width: "80%",
              margin: "0 auto",
              position: "absolute",
              left: 0,
              right: 0,
              bottom: "10%",
              textAlign: "center",
              color: "white",
            }}
          >
            Submit Job
          </Button>
        )}
        {current > 0 && (
          <Button
            style={{
              backgroundColor: "#0276C0",
              width: "80%",

              margin: "0 auto",
              position: "absolute",
              left: 0,
              right: 0,
              bottom: "15%",
              textAlign: "center",
              color: "white",
            }}
            onClick={() => prev()}
          >
            Previous
          </Button>
        )}
      </div>

      <div>
        <Modal
          visible={isModalVisible}
          onCancel={handleClose}
          footer={[
            <div key="footer-content">
            <Button 
              key="close" 
              type="primary" 
              onClick={() => navigate('/job-submitted')}
              style={{ 
                display: 'block', 
                margin: 'auto', 
                width: '252px',
                height: '40px',
                backgroundColor: '#0276C0'
              }}
            >
              Confirm Submission
            </Button>
            <p style={{ textAlign: 'justify', marginTop: '10px', fontSize: '9px', margin: '10px', color: '#979797'}}>By clicking "Confirm Submission," you acknowledge that the information provided is accurate, and you consent to receiving job status updates via the provided email.</p>
          </div>
          ]}
        >
          <div>
            <div style={{ display: 'block' }}>
              <img src={ddmovelogo} alt='ddmovelogo' style={{ height: '41px', width: '123px', verticalAlign: 'top' }} />
            </div>
            <div style={{ display: 'block' }}>
              <img src={blueLine} alt='line' style={{ width: '32px', float: 'left', verticalAlign: 'top', marginTop: '10px' }} />
            </div>
            <div style={{ display: 'block' }}>
              <h1 style={{ fontSize: '10px', color: 'black', marginTop: '40px'}}>
                To complete your job submission, please enter your contact details:
              </h1>
            </div>
          </div>

          <div style={{padding: '10px', width: '300px' }}>
          <form>
              <input 
                type="text" 
                value={name} 
                onChange={handleChange}
                placeholder='Your name' 
                style={{ width: '110%', padding: '5px', marginTop: '20px', boxSizing: 'border-box', border: '1px solid #CCCCCC', borderRadius: '5px', height: '41px'}} 
              />

              <Input 
                addonBefore="+60"
                bordered={false}
                type="tel" 
                value={number} 
                onChange={handleChange}
                placeholder='Mobile Number' 
                style={{ width: '110%', padding: '5px', marginTop: '20px', boxSizing: 'border-box', border: '1px solid #CCCCCC', borderRadius: '5px', height: '41px'}} 
              />

              <input 
                type="email" 
                value={email} 
                onChange={handleChange}
                placeholder='Email' 
                style={{ width: '110%', padding: '5px', marginTop: '20px', boxSizing: 'border-box', border: '1px solid #CCCCCC', borderRadius: '5px', height: '41px' }} 
              />
          </form>
        </div>
        </Modal>
      </div>
    </>
  );
};

export default SubmitJob;
